import Head from 'next/head'
import Button from '@/components/shared/Button/Button'
import Navigation from '@/components/shared/Navigation/Navigation'

export default function Home() {
  return (
    <>
      <Head>
        <title>Hello World</title>
      </Head>
      <main>
        <Navigation />
        <h1>Hello world!</h1>
        <Button primary>Primary</Button>
      </main>
    </>
  )
}

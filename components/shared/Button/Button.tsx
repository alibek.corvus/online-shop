import React, { FC } from 'react'
import clsx from 'clsx'
import styles from './Button.module.scss'

interface IProps {
  primary: boolean
  children: string
}
const Button: FC<IProps> = ({ primary, children }) => {
  return (
    <button
      className={clsx({
        [styles.buttonPrimary]: primary
      })}
    >
      {children}
    </button>
  )
}

export default Button

import { ComponentMeta, ComponentStory } from '@storybook/react'

import Navigation from './Navigation'

export default {
  title: 'Navigation',
  component: Navigation
} as ComponentMeta<typeof Navigation>

export const NavigationComponent: ComponentStory<typeof Navigation> = () => (
  <Navigation />
)

import React, { FC } from 'react'
import styles from './Navigation.module.scss'
import NavigationItem from '@/components/shared/Navigation/NavigationItem/NavigationItem'

export type TMenuItem = { id: string; product: string; label: string }
const navigationItems: Array<TMenuItem> = [
  {
    id: 'f884e530-3369-4e2d-97f3-0fd2551b0c6b',
    product: 'smartphones',
    label: 'Телефоны'
  },
  {
    id: 'a18b447c-375e-4bf4-a7b6-f4391610147c',
    product: 'tablets',
    label: 'Планшеты'
  },
  {
    id: '4eb4ad8d-51db-4bd4-a402-3ee547e777f6',
    product: 'watches',
    label: 'Часы'
  }
]

const Navigation: FC = () => {
  return (
    <div className={styles.navigation}>
      <div className={styles.navigationProducts}>
        {navigationItems.map((item) => (
          <NavigationItem
            product={item.product}
            key={item.id}
            label={item.label}
          />
        ))}
      </div>
      <div>
        <NavigationItem product="basket" label="Basket" />
      </div>
    </div>
  )
}

export default Navigation

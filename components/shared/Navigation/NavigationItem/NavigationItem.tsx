import React, { FC } from 'react'
import { TMenuItem } from '@/components/shared/Navigation/Navigation'
import styles from './NavigationItem.module.scss'
import Link from 'next/link'

type TProps = Omit<TMenuItem, 'id'>
const NavigationItem: FC<TProps> = ({ label, product }) => {
  return (
    <Link href={`/${product}`} className={styles.link}>
      {label}
    </Link>
  )
}

export default NavigationItem
